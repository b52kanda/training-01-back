import { createLogger, format, transports } from 'winston';

const { combine, timestamp, printf } = format;

let logger = null;

function createWinstonLogger() {
  const winstonLogger = createLogger({
    format: combine(
      timestamp(),
      printf(info => `${info.timestamp} | ${info.level.toUpperCase()} | ${info.message}`),
    ),
    transports: [
      new transports.File({ filename: 'error.log', level: 'error' }),
      new transports.File({ filename: 'combined.log' }),
    ],
  });

  if (process.env.NODE_ENV !== 'production') {
    winstonLogger.add(new transports.Console());
  }

  return winstonLogger;
}

export default function getLogger() {
  if (!logger) {
    logger = createWinstonLogger();
  }

  return logger;
}
