export default function (request) {
  const { headers: { authorization = '' } } = request;
  const [prefix, token] = authorization.split(' ');

  return prefix === 'Token' ? token : null;
}
