import mongoose from 'mongoose';

const { Schema } = mongoose;

const TeamSchema = new Schema({
  name: String,
  city: String,
  players: [{ type: Schema.Types.ObjectId, ref: 'Player' }],
  linkToLogo: String,
});

TeamSchema.methods.getTeamDTO = function getTeamDTO() {
  return {
    _id: this._id,
    name: this.name,
    city: this.city,
    players: this.players,
    linkToLogo: this.linkToLogo,
  };
};

export default mongoose.model('Team', TeamSchema);
