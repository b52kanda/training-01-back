import mongoose from 'mongoose';

const { Schema } = mongoose;

const ChampionshipSchema = new Schema({
  name: String,
  teams: [{
    team: { type: Schema.Types.ObjectId, ref: 'Team' },
    stats: {
      points: Number,
      wins: Number,
      draws: Number,
      defeats: Number,
      goals: Number,
      missedGoals: Number,
    },
  }],
  matches: [{ type: Schema.Types.ObjectId, ref: 'Match' }],
  isActive: Boolean,
});

ChampionshipSchema.methods.getChampionshipDTO = function getChampionshipDTO() {
  return {
    _id: this._id,
    name: this.name,
    teams: this.teams,
    matches: this.matches,
    isActive: this.isActive,
  };
};

export default mongoose.model('Championship', ChampionshipSchema);
