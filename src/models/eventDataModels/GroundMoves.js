import mongoose from 'mongoose';

const { Schema } = mongoose;

export const groundMovesResults = {
  dodge: 'DODGE',
  dodged: 'DODGED',
  steal: 'STEAL',
  stealed: 'STEALED',
};

const GroundMovesSchema = new Schema({
  player: { type: Schema.Types.ObjectId, ref: 'Player' },
  result: { type: String, enum: Object.values(groundMovesResults) },
});

GroundMovesSchema.methods.getGroundMovesDTO = function getGroundMovesDTO() {
  return {
    _id: this._id,
    player: this.player,
    result: this.result,
  };
};

export default mongoose.model('GroundMoves', GroundMovesSchema);
