import mongoose from 'mongoose';

const { Schema } = mongoose;

export const kickDistances = {
  long: 'LONG',
  average: 'AVERAGE',
  short: 'SHORT',
};

export const kickSources = {
  head: 'HEAD',
  leg: 'LEG',
};

export const kickResults = {
  goal: 'GOAL',
  block: 'BLOCK',
  miss: 'MISS',
  above: 'ABOVE',
};

const KickSchema = new Schema({
  player: { type: Schema.Types.ObjectId, ref: 'Player' },
  distance: { type: String, enum: Object.values(kickDistances) },
  source: { type: String, enum: Object.values(kickSources) },
  result: { type: String, enum: Object.values(kickResults) },
});

KickSchema.methods.getKickDTO = function getKickDTO() {
  return {
    _id: this._id,
    player: this.player,
    distance: this.distance,
    source: this.source,
    result: this.result,
  };
};

export default mongoose.model('Kick', KickSchema);
