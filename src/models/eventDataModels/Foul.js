import mongoose from 'mongoose';

const { Schema } = mongoose;

const FoulSchema = new Schema({
  violator: { type: Schema.Types.ObjectId, ref: 'Player' },
  fouledPlayer: { type: Schema.Types.ObjectId, ref: 'Player' },
  card: { type: Schema.Types.ObjectId, ref: 'Card' },
});

FoulSchema.methods.getFoulDTO = function getFoulDTO() {
  return {
    _id: this._id,
    violator: this.violator,
    fouledPlayer: this.fouledPlayer,
    card: this.card,
  };
};

export default mongoose.model('Foul', FoulSchema);
