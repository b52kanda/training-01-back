import mongoose from 'mongoose';

const { Schema } = mongoose;

const SubstituteSchema = new Schema({
  replacedPlayer: { type: Schema.Types.ObjectId, ref: 'Player' },
  benchwarmer: { type: Schema.Types.ObjectId, ref: 'Player' },
});

SubstituteSchema.methods.getSubstituteDTO = function getSubstituteDTO() {
  return {
    _id: this._id,
    replacedPlayer: this.replacedPlayer,
    benchwarmer: this.benchwarmer,
  };
};

export default mongoose.model('Substitute', SubstituteSchema);
