import mongoose from 'mongoose';

const { Schema } = mongoose;

export const passDistances = {
  long: 'LONG',
  average: 'AVERAGE',
  short: 'SHORT',
};

export const passDirections = {
  forward: 'FORWARD',
  back: 'BACK',
  acrossTheField: 'ACROSS_THE_FIELD',
};

export const passAccuracy = {
  accurate: 'ACCURATE',
  inaccurate: 'INACCURATE',
};

const PassSchema = new Schema({
  sourcePlayer: { type: Schema.Types.ObjectId, ref: 'Player' },
  targetPlayer: { type: Schema.Types.ObjectId, ref: 'Player' },
  distance: { type: String, enum: Object.values(passDistances) },
  direction: { type: String, enum: Object.values(passDirections) },
  accuracy: { type: String, enum: Object.values(passAccuracy) },
});

PassSchema.methods.getPassDTO = function getPassDTO() {
  return {
    _id: this._id,
    sourcePlayer: this.sourcePlayer,
    targetPlayer: this.targetPlayer,
    distance: this.distance,
    direction: this.direction,
    accuracy: this.accuracy,
  };
};

export default mongoose.model('Pass', PassSchema);
