import mongoose from 'mongoose';
import { playerPositions } from './Player';

const { Schema } = mongoose;

export const eventDataModelsNames = ['Foul', 'GroundMoves', 'Substitute', 'Pass', 'Kick'];

const MatchSchema = new Schema({
  homePlayingTeam: {
    team: { type: Schema.Types.ObjectId, ref: 'Team' },
    mainStructure: [
      {
        player: { type: Schema.Types.ObjectId, ref: 'Player' },
        position: { type: String, enum: Object.values(playerPositions) },
      },
    ],
    benchwarmers: { type: Schema.Types.ObjectId, ref: 'Player' },
    goals: Number,
  },
  guestTeam: {
    team: { type: Schema.Types.ObjectId, ref: 'Team' },
    mainStructure: [
      {
        player: { type: Schema.Types.ObjectId, ref: 'Player' },
        position: { type: String, enum: Object.values(playerPositions) },
      },
    ],
    benchwarmers: { type: Schema.Types.ObjectId, ref: 'Player' },
    goals: Number,
  },
  round: Number,
  date: Date,
  gameCity: String,
  messages: [
    {
      time: Number,
      message: String,
    },
  ],
  events: [
    {
      time: Number,
      data: { type: Schema.Types.ObjectId, refPath: 'eventModelName' },
      eventModelName: { type: String, enum: eventDataModelsNames },
    },
  ],
  isCompleted: Boolean,
});

MatchSchema.methods.getMatchDTO = function getMatchDTO() {
  return {
    _id: this._id,
    homePlayingTeam: this.homePlayingTeam,
    guestTeam: this.guestTeam,
    round: this.round,
    date: this.date,
    gameCity: this.gameCity,
    messages: this.messages,
    events: this.events,
    isCompleted: this.isCompleted,
  };
};

MatchSchema.methods.getLastCompletedMatchInfo = function getLastCompletedMatchInfo() {
  return {
    matchId: this._id,
    allPlayers: this.homePlayingTeam.mainStructure.concat(this.guestTeam.mainStructure),
    teamNames: [this.homePlayingTeam.team.name, this.guestTeam.team.name],
  };
};

MatchSchema.methods.getMatchResult = function getMatchResult() {
  return {
    _id: this._id,
    homePlayingTeam:
      {
        name: this.homePlayingTeam.team.name,
        linkToLogo: this.homePlayingTeam.team.linkToLogo,
        goals: this.homePlayingTeam.goals,
      },
    guestTeam:
      {
        name: this.guestTeam.team.name,
        linkToLogo: this.guestTeam.team.linkToLogo,
        goals: this.guestTeam.goals,
      },
  };
};

export default mongoose.model('Match', MatchSchema);
