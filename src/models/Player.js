import mongoose from 'mongoose';

const { Schema } = mongoose;

export const playerPositions = {
  goalkeeper: 'POSITION_GOALKEEPER',
  rightDefender: 'POSITION_RIGHT_DEFENDER',
  leftDefender: 'POSITION_LEFT_DEFENDER',
  centralDefender: 'POSITION_CENTRAL_DEFENDER',
  centralDefensiveMidfielder: 'POSITION_CENTRAL_DEFENSIVE_MIDFIELDER',
  rightMidfielder: 'POSITION_RIGHT_MIDFIELDER',
  centralMidfielder: 'POSITION_CENTRAL_MIDFIELDER',
  leftMidfielder: 'POSITION_LEFT_MIDFIELDER',
  rightSideMidfielder: 'POSITION_RIGHT_SIDE_MIDFIELDER',
  leftSideMidfielder: 'POSITION_LEFT_SIDE_MIDFIELDER',
  centralAttackingMidfielder: 'POSITION_CENTRAL_ATTACKING_MIDFIELDER',
  striker: 'POSITION_STRICKER',
};

const PlayerSchema = new Schema({
  firstName: String,
  lastName: String,
  number: Number,
  defaultPosition: { type: String, enum: Object.values(playerPositions) },
  photoLink: String,
  stats: [
    {
      match: { type: Schema.Types.ObjectId, ref: 'Match' },
      goals: Number,
      assists: Number,
      cards: [{ type: Schema.Types.ObjectId, ref: 'Card' }],
      minutesPlayed: Number,
      substitutionMinute: Number,
      substitutionedMinute: Number,
      isMatchPlayedCompletely: Boolean,
      fansVotes: Number,
    },
  ],
});

PlayerSchema.methods.getPlayerDTO = function getPlayerDTO() {
  return {
    _id: this._id,
    firstName: this.firstName,
    lastName: this.lastName,
    number: this.number,
    defaultPosition: this.defaultPosition,
    photoLink: this.photoLink,
    stats: this.stats,
  };
};

export default mongoose.model('Player', PlayerSchema);
