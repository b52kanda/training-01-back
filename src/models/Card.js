import mongoose from 'mongoose';

const { Schema } = mongoose;

export const cardTypes = {
  red: 'RED',
  yellow: 'YELLOW',
};

const CardSchema = new Schema({
  time: Number,
  type: { type: String, enum: cardTypes },
  player: { type: Schema.Types.ObjectId, ref: 'Player' },
  reason: String,
});

CardSchema.methods.getCardDTO = function getCardDTO() {
  return {
    _id: this._id,
    time: this.time,
    type: this.type,
    player: this.player,
    reason: this.reason,
  };
};

export default mongoose.model('Card', CardSchema);
