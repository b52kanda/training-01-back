import mongoose from 'mongoose';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';

const { Schema } = mongoose;

export const userRoles = {
  admin: 'ROLE_ADMIN',
  coach: 'ROLE_COACH',
};

const UserSchema = new Schema({
  email: { type: String, unique: true, dropDups: true },
  hash: String,
  salt: String,
  role: { type: String, enum: Object.values(userRoles) },
  isActive: { type: Boolean, default: false },
});

const generateHash = (password, salt) => crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('hex');

UserSchema.methods.setPassword = function setPassword(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = generateHash(password, this.salt);
};

UserSchema.methods.validatePassword = function validatePassword(password) {
  return this.hash === generateHash(password, this.salt);
};

UserSchema.methods.generateJWT = function generateJWT() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
    id: this._id,
    email: this.email,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
};

UserSchema.methods.toAuthJSON = function toAuthJSON() {
  return {
    ...this.getUserDTO(),
    token: this.generateJWT(),
  };
};

UserSchema.methods.getUserDTO = function getUserDTO() {
  return {
    _id: this._id,
    email: this.email,
    role: this.role,
    isActive: this.isActive,
  };
};

export default mongoose.model('User', UserSchema);
