import User, { userRoles } from 'src/models/User';
import ServerError from 'src/errors/ServerError';

export default async (request, response, next) => {
  const { payload: { id } } = request;

  const user = await User.findById(id);

  if (!user || user.role !== userRoles.admin) {
    return next(new ServerError('User should have an admin role', 403));
  }

  return next();
};
