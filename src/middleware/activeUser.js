import User from 'src/models/User';
import ServerError from 'src/errors/ServerError';

export default async (request, response, next) => {
  const { payload: { id } } = request;

  const user = await User.findById(id);

  if (!user || !user.isActive) {
    return next(new ServerError('User is inactive', 403));
  }

  return next();
};
