import jwt from 'express-jwt';
import getToken from 'utils/getToken';

export default jwt({
  secret: 'secret',
  userProperty: 'payload',
  getToken,
});
