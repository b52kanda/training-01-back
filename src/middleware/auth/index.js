import optional from './optional';
import required from './required';

export default {
  optional,
  required,
};
