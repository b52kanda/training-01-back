import expressValidatorCheck from 'express-validator/check';

const { validationResult } = expressValidatorCheck;

export default (request, response, next) => {
  const errors = validationResult(request);

  if (!errors.isEmpty()) {
    return response.status(422).json({ errors: errors.array() });
  }

  return next();
};
