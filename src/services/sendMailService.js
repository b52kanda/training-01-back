import nodemailer from 'nodemailer';
import config from '../../boot/config';

const createTransport = () => nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.email.user,
    pass: config.email.password,
  },
});

const buildMailOptions = (recipient, newPassword) => ({
  from: config.email.user,
  to: recipient,
  subject: 'Reset your password',
  html: '<div>'
  + '<p>You have requested a password reset, please use a new password.</p>'
  + '<p>Please ignore this email if you did not request a password change</p>'
  + `<p>New password: ${newPassword}</p>`
  + '<p><a href="http://b52-training01.s3-website.us-east-2.amazonaws.com/login">Login page</a></p>'
  + '</div>',
});

export default async (recipient, newPassword, next) => {
  const transporter = createTransport();

  await transporter.sendMail(buildMailOptions(recipient, newPassword), error => next(error));
};
