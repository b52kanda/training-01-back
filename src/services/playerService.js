import Player from 'src/models/Player';

import ServerError from 'src/errors/ServerError';

const addVoteByPlayerAndMatchIds = async (playerId, matchId) => {
  const player = await Player.findOneAndUpdate(
    { _id: playerId, 'stats.match': matchId },
    {
      $inc: {
        'stats.$.fansVotes': 1,
      },
    },
    { new: true },
  );

  if (!player) {
    throw new ServerError('Can\'t find player or match');
  }

  return player;
};

export default {
  addVoteByPlayerAndMatchIds,
};
