import generator from 'generate-password';

export default () => generator.generate({
  length: 6,
  numbers: true,
});
