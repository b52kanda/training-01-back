import User from 'src/models/User';
import ServerError from 'src/errors/ServerError';

const getUserById = async (id) => {
  const user = await User.findById(id);

  if (!user) {
    throw new ServerError('User doesn\'t exists');
  }

  return user;
};

const getActiveUserByEmail = async (email) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new ServerError('User doesn\'t exists');
  }

  if (!user.isActive) {
    throw new ServerError('User is not active');
  }

  return user;
};

const createUser = async (userRaw) => {
  const user = new User(userRaw);
  user.setPassword(userRaw.password);
  await user.save();

  return user;
};

const getUsers = async () => {
  const users = User.find({});

  return users;
};

export default {
  getUserById,
  getActiveUserByEmail,
  getUsers,
  createUser,
};
