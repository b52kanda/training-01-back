import Match from 'src/models/Match';
import Team from 'src/models/Team'; // eslint-disable-line no-unused-vars
import Player from 'src/models/Player'; // eslint-disable-line no-unused-vars

import ServerError from 'src/errors/ServerError';

const getLastCompletedMatch = async () => {
  const matches = await Match.find({ isCompleted: true })
    .sort({ date: -1 })
    .limit(1)
    .populate('homePlayingTeam.team')
    .populate('guestTeam.team')
    .populate('homePlayingTeam.mainStructure.player')
    .populate('guestTeam.mainStructure.player');

  if (!matches) {
    throw new ServerError('Completed matches don\'t exist');
  }

  return matches[0];
};

const getAllCompletedMatchesResults = async () => {
  const matches = await Match.find({ isCompleted: true })
    .sort({ date: -1 })
    .populate('homePlayingTeam.team')
    .populate('guestTeam.team');

  if (!matches) {
    throw new ServerError('Completed matches don\'t exist');
  }

  return matches;
};

const getClosestMatch = async () => {
  const match = await Match.find({ isCompleted: false })
    .sort({ date: 1 })
    .limit(1)
    .populate('homePlayingTeam.team')
    .populate('guestTeam.team');

  if (!match) {
    throw new ServerError('Non completed matches don\'t exist');
  }

  return match[0];
};

export default {
  getLastCompletedMatch,
  getAllCompletedMatchesResults,
  getClosestMatch,
};
