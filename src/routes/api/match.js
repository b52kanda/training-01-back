import { Router } from 'express';
import auth from 'src/middleware/auth';
import {
  getLastCompletedMatchInfo,
  getAllCompletedMatchesResults,
  getClosestMatch,
} from 'src/controllers/match';

const router = Router();

router.get('/lastCompleted', auth.optional, getLastCompletedMatchInfo);
router.get('/results', auth.optional, getAllCompletedMatchesResults);
router.get('/closestMatch', auth.optional, getClosestMatch);

export default router;
