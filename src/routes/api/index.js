import { Router } from 'express';
import user from './user';
import match from './match';
import player from './player';

const router = Router();

router.use('/user', user);
router.use('/match', match);
router.use('/player', player);

export default router;
