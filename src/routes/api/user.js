import { Router } from 'express';
import { check } from 'express-validator/check';

import auth from 'src/middleware/auth';
import activeUser from 'src/middleware/activeUser';
import adminUser from 'src/middleware/adminUser';
import checkValidation from 'src/middleware/checkValidation';

import userController from 'src/controllers/user';
import { userRoles } from 'src/models/User';

const router = Router();

router.post('/', auth.optional, [
  check('user.email').exists().withMessage('email is required'),
  check('user.password').exists().withMessage('password is required'),
  check('user.role').isIn(Object.values(userRoles)).withMessage('role should be an item from enum'),
], checkValidation, userController.createUser);

router.post('/login', auth.optional, [
  check('user.email').exists().withMessage('email is required'),
  check('user.password').exists().withMessage('password is required'),
], checkValidation, userController.loginUser);

router.post('/activate', auth.required, activeUser, adminUser, [
  check('user.id').exists().withMessage('user.id is required to perform this action'),
], checkValidation, userController.activateUser);

router.get('/current', auth.required, activeUser, userController.getUser);

router.get('/', auth.required, activeUser, adminUser, userController.getUsers);

router.post('/changePassword', auth.required, activeUser, [
  check('user.password').exists().withMessage('password is required'),
], checkValidation, userController.changePassword);

router.post('/resetPassword', auth.optional, [
  check('user.email').exists().withMessage('email is required'),
], checkValidation, userController.resetPassword);

export default router;
