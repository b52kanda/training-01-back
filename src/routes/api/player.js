import { Router } from 'express';
import auth from 'src/middleware/auth';
import { addVoteByPlayerAndMatchIds } from 'src/controllers/player';

const router = Router();

router.put('/vote/:playerId/:matchId', auth.optional, addVoteByPlayerAndMatchIds);

export default router;
