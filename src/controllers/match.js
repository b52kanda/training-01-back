import matchService from 'src/services/matchService';

export const getLastCompletedMatchInfo = async (request, response, next) => {
  try {
    const lastCompletedMatch = await matchService.getLastCompletedMatch();

    return response.json({
      lastCompletedMatchInfo: lastCompletedMatch.getLastCompletedMatchInfo(),
    });
  } catch (error) {
    return next(error);
  }
};

export const getAllCompletedMatchesResults = async (request, response, next) => {
  try {
    const completedMatches = await matchService.getAllCompletedMatchesResults();

    return response.json({
      completedMatchesResults: completedMatches.map(match => match.getMatchResult()),
    });
  } catch (error) {
    return next(error);
  }
};

export const getClosestMatch = async (request, response, next) => {
  try {
    const closestMatch = await matchService.getClosestMatch();

    return response.json({
      closestMatch: closestMatch.getMatchDTO(),
    });
  } catch (error) {
    return next(error);
  }
};

export default {
  getLastCompletedMatchInfo,
  getAllCompletedMatchesResults,
};
