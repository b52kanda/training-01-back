import playerService from 'src/services/playerService';

export const addVoteByPlayerAndMatchIds = async (request, response, next) => {
  const {
    params: { playerId, matchId },
  } = request;

  try {
    const player = await playerService.addVoteByPlayerAndMatchIds(playerId, matchId);

    return response.json({ player: player.getPlayerDTO() });
  } catch (error) {
    return next(error);
  }
};

export default {
  addVoteByPlayerAndMatchIds,
};
