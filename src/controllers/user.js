import passport from 'passport';
import ServerError from 'src/errors/ServerError';
import userService from 'src/services/userService';
import sendMail from 'src/services/sendMailService';
import generatePassword from 'src/services/generatePasswordService';

const createUser = async (request, response, next) => {
  const { body: { user: userRaw } } = request;

  try {
    const user = await userService.createUser(userRaw);

    return response.json({ user: user.getUserDTO() });
  } catch (error) {
    return next(error);
  }
};

const getUser = async (request, response, next) => {
  const { payload: { id } } = request;

  try {
    const user = await userService.getUserById(id);

    return response.json({ user: user.toAuthJSON() });
  } catch (error) {
    return next(error);
  }
};

const getUsers = async (request, response, next) => {
  try {
    const users = await userService.getUsers();

    return response.json({ users: users.map(user => user.getUserDTO()) });
  } catch (error) {
    return next(error);
  }
};

const loginUser = (request, response, next) => passport.authenticate('local', { session: false }, (err, user, info) => {
  if (err) {
    return next(err);
  }

  if (user) {
    return response.json({ user: user.toAuthJSON() });
  }

  return next(info);
})(request, response, next);

const activateUser = async (request, response, next) => {
  const { body: { user: { id } } } = request;

  try {
    const user = await userService.getUserById(id);

    if (user.isActive) throw new ServerError('User already active');

    user.isActive = true;
    await user.save();

    return response.json({ user: user.getUserDTO() });
  } catch (error) {
    return next(error);
  }
};

const changePassword = async (request, response, next) => {
  const { payload: { id }, body: { user: { password } } } = request;

  try {
    const user = await userService.getUserById(id);

    user.setPassword(password);
    await user.save();

    return response.json({ user: user.getUserDTO() });
  } catch (error) {
    return next(error);
  }
};

const resetPassword = async (request, response, next) => {
  const { body: { user: { email } } } = request;

  try {
    const user = await userService.getActiveUserByEmail(email);
    const newPassword = generatePassword();
    user.setPassword(newPassword);
    await sendMail(email, newPassword, next);
    await user.save();

    return response.json({ user: { email } });
  } catch (error) {
    return next(error);
  }
};

export default {
  getUser, getUsers, createUser, loginUser, activateUser, changePassword, resetPassword,
};
