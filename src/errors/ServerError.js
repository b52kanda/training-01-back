export default class extends Error {
  constructor(message, httpCode = 400) {
    super(message);

    this.httpCode = httpCode;
  }

  toJSON() {
    return { message: this.message };
  }
}
