import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import expressSession from 'express-session';
import cors from 'cors';
import getLogger from 'utils/logger';
import routes from 'src/routes';
import corsOptions from 'boot/cors';
import config from 'boot/config';
import connectToDatabase from 'boot/db';
import setupPassport from 'boot/passport';

const app = express();
const { env: { port } } = config;

app.use(cors(corsOptions));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
  secret: 'b52-training',
  cookie: { maxAge: 60000 },
  resave: false,
  saveUninitialized: false,
}));

connectToDatabase();
setupPassport();
app.use(routes);

app.use((error, request, response, next) => {
  if (error) {
    getLogger().error(error.message);

    return response.status(error.httpCode || 400).json({ errors: [error] });
  }

  return next();
});

app.listen(port, () => {
  getLogger().info(`We are live on ${port}`);
});
