const whitelist = ['http://localhost:3000', 'http://b52-training01.s3-website.us-east-2.amazonaws.com'];

const corsOptions = {
  origin: (origin, callback) => {
    if (!origin || whitelist.includes(origin)) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

export default corsOptions;
