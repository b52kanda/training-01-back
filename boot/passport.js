import passport from 'passport';
import LocalStrategy from 'passport-local';
import userService from 'src/services/userService';
import ServerError from 'src/errors/ServerError';

export default function setupPassport() {
  passport.use(new LocalStrategy({
    usernameField: 'user[email]',
    passwordField: 'user[password]',
  }, async (email, password, done) => {
    try {
      const user = await userService.getActiveUserByEmail(email);

      if (!user.validatePassword(password)) throw new ServerError('email or password is invalid');

      return done(null, user);
    } catch (error) {
      return done(null, false, error);
    }
  }));
}
