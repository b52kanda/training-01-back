const config = {
  db: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST || 'ds131932.mlab.com',
    port: process.env.DB_PORT || 31932,
    name: process.env.DB_NAME || 'training-01',
  },
  env: {
    port: process.env.PORT || 8000,
  },
  email: {
    user: process.env.EMAIL,
    password: process.env.EMAIL_PASSWORD,
  },
};

export default config;
