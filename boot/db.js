import mongoose from 'mongoose';
import config from './config';

const {
  db: {
    username, password, host, port, name,
  },
} = config;
const connectionURI = `mongodb://${username}:${password}@${host}:${port}/${name}`;

const connectionOptions = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

const connectToDatabase = () => {
  mongoose.connect(
    connectionURI,
    connectionOptions,
  );
  mongoose.set('debug', process.env.NODE_ENV !== 'production');
};

export default connectToDatabase;
