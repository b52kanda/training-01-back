module.exports = {
  extends: 'airbnb-base',
  rules: {
    'no-underscore-dangle': [2, { allow: ['_id'] }]
  },
  settings: {
    'import/resolver': {
      alias: [
        ['src', './src'],
        ['utils', './utils'],
        ['boot', './boot'],
      ]
    }
  }
};